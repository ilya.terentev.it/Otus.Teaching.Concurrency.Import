﻿using System;
using System.IO;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    class Program
    {
        private static readonly string DataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFileName; 
        private static int _dataCount = 100;
        private static GeneratorType _dataGeneratorType;

        static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;

            Console.WriteLine("Generating xml data...");

            var generator = GeneratorFactory.GetGenerator(_dataGeneratorType, _dataFileName, _dataCount);
            generator.Generate();

            Console.WriteLine($"Generated xml data in {_dataFileName}...");
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                _dataFileName = Path.Combine(DataFileDirectory, $"{args[0]}.xml");
            }
            else
            {
                Console.WriteLine("Data file name without extension is required");
                return false;
            }

            if (args.Length < 2 || !int.TryParse(args[1], out _dataCount))
            {
                Console.WriteLine("Data must be integer");
                return false;
            }

            if (args.Length < 3 || !Enum.TryParse(args[2], out _dataGeneratorType))
            {
                Console.WriteLine("The generator type is not set");
                return false;
            }

            return true;
        }
    }
}
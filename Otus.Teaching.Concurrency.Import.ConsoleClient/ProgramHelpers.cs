﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.ConsoleClient
{
    internal partial class Program
    {
        public static void InitSettings()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();
            _settings = config.GetRequiredSection("Settings").Get<Settings>();
        }

        public static string GetInitText()
        {
            var sb = new StringBuilder();
            sb.Append($"Select action:{Environment.NewLine}");
            sb.Append($"\tEnter 'Q' for quit {Environment.NewLine}");
            sb.Append($"\tEnter 'CLR' for clear console {Environment.NewLine}");
            sb.Append($"\tEnter '2' for post random customer{Environment.NewLine}");
            sb.Append($"\tEnter '1' for get customer");
            return sb.ToString();
        }

        public static async Task GetCustomerById(Api api, int id)
        {
            var customer = await api.GetDataFromApi<Customer>($"{Path.Combine(_settings.ApiPath, $"{id}")}");
            ConsoleExtensions.ShowCustomer(customer);
        }

        public static async Task GenerateRandomCustomer(Api api, Settings settings)
        {
            var customers = RandomCustomerGenerator.Generate(1);
            var newCustomer = customers[0];
            var r = new Random();
            newCustomer.Id = r.Next(40, 100);
            ConsoleExtensions.ShowCustomer(newCustomer, "generated");

            await api.AddDataToApi("webApi/users", newCustomer);
            var customer =
                await api.GetDataFromApi<Customer>($"{Path.Combine(settings.ApiPath, $"{newCustomer.Id}")}");
            ConsoleExtensions.ShowCustomer(customer, "added successfully");
        }
    }
}

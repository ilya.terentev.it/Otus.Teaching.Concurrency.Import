﻿using System;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.ConsoleClient
{
    public static class ConsoleExtensions
    {
        public static void ShowCustomer(Customer product, string additional ="")
        {
            Console.WriteLine($"Customer: {product} {additional}");
        }

        public static void ShowError(string e)
        {
            var old = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(e);
            Console.ForegroundColor = old;
        }
        public static void ShowQuestion(string msg)
        {
            var old = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(msg);
            Console.ForegroundColor = old;
        }
    }
}
﻿namespace Otus.Teaching.Concurrency.Import.ConsoleClient
{
    public class Settings
    {
        public string ApiPath { get; set; }
        public string ServerAddress { get; set; }
    }
}
﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.ConsoleClient
{
    internal partial class Program
    {
        private static Settings _settings;

        static async Task Main()
        {
            try
            {
                InitSettings();
                var api = new Api(_settings.ServerAddress);
                var q = false;
                var initText = GetInitText();
                do
                {
                    try
                    {
                        ConsoleExtensions.ShowQuestion(initText);
                        var act = Console.ReadLine();
                        switch (act?.ToLower())
                        {
                            case "1":
                                ConsoleExtensions.ShowQuestion("Enter customer id");
                                var str = Console.ReadLine();
                                if (int.TryParse(str, out int id))
                                    await GetCustomerById(api, id);
                                else
                                    ConsoleExtensions.ShowError($"{str} is not correct type for id!");
                                break;
                            case "2":
                                await GenerateRandomCustomer(api, _settings);
                                break;
                            case "q":
                                q = true;
                                break;
                            case "clr":
                                Console.Clear();
                                break;
                            default:
                                continue;
                        }
                    }
                    catch (Exception e)
                    {
                        ConsoleExtensions.ShowError(e.Message);
                    }
                } while (!q);
            }
            catch (Exception e)
            {
                ConsoleExtensions.ShowError(e.Message);
            }
        }
    }
}
﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.ConsoleClient
{
    public class Api
    {
        private readonly HttpClient _client;

        public Api(string url)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(url);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<T> GetDataFromApi<T>(string path)
        {
            var response = await _client.GetAsync(path);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadFromJsonAsync<T>();
        }

        public async Task AddDataToApi(string path, object data)
        {
            var response = await _client.PostAsJsonAsync(path, data);
            response.EnsureSuccessStatusCode();
        }
    }
}
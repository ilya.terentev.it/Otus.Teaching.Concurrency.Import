﻿using System;
using System.IO;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using ServiceStack.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class JsonRepository: ICustomerRepository
    {
        private readonly string _fileName;
        private readonly object _lock = new object();

        public JsonRepository(string fileName)
        {
            _fileName = fileName;
        }

        public void AddCustomer(Customer customer, int tryCounter = 0 )
        {
            try
            {
                var s = JsonSerializer.SerializeToString(customer, typeof(Customer));
                lock (_lock)
                {
                    File.AppendAllText(_fileName, s);
                }
            }
            catch (Exception ex)
            {
                if (tryCounter < 3)
                    AddCustomer(customer, tryCounter++);
                else
                    throw new InvalidOperationException($"Error on add customer {customer.ToString()}", ex);
            }
        }
    }
}

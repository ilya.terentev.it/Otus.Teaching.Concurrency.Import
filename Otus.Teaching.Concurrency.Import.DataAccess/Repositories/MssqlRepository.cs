using System;
using System.Data.SqlClient;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class MssqlRepository : ICustomerRepository
    {

        private string _connectionString;

        public MssqlRepository(string connectionString)
        {
            _connectionString = connectionString;
#if DEBUG
            ClearForDebug();
#endif 
        }

        public void AddCustomer(Customer customer, int tryCounter = 0)
        {
            try
            {
                using var connection = new SqlConnection(_connectionString);
                connection.Open();
                //var transaction = connection.BeginTransaction();
                try
                {
                    // �������� ���������� � �������� ��, � �������� �� ������� SQL Lite ��� �������� ������������ � ����������.
                    //�� ������� ������ SQL Lite ��������� ������ ���������� �� ����������� ��, ��������, MS SQL Server ��� PostgreSQL, �������� ������ ����� �� 3 ��� �������. ��� ����� ������ �������� � �����������, ��������� �� ���������� ���������� ��� ������ � ������� � �� ����� ����� ����������� ��������.(*) 
                    var queryString = $"INSERT INTO OtusCustmersDb.dbo.Customers values(@p1,@p2,@p3,@p4) ";
                    var command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@p1", customer.Id);
                    command.Parameters.AddWithValue("@p2", customer.FullName);
                    command.Parameters.AddWithValue("@p3", customer.Email);
                    command.Parameters.AddWithValue("@p4", customer.Phone);

                    var r = command.ExecuteNonQuery();
                    //transaction.Commit();
                }
                catch (Exception ex)
                {
                    try
                    {
                      //  transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                    }
                    //������������� ��������� ������ � �������� ������� ��� ���������� ���������� � ������ ������.
                    if (tryCounter < 3)
                        AddCustomer(customer, tryCounter++);
                    else
                        throw new InvalidOperationException($"Error on add customer {customer}", ex);
                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException($"Error on connection open {_connectionString}", e);
            }
            // Console.ReadLine();
        }

        private void ClearForDebug()
        {
            using var connection = new SqlConnection(_connectionString);
            var queryString = $"TRUNCATE table [OtusCustmersDb].[dbo].[Customers]";
            var command = new SqlCommand(queryString, connection);
            try
            {
                connection.Open();
                _ = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }
        }
    }
}
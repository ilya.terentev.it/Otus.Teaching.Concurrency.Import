﻿using System.Collections.Generic;   
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        private readonly string  _fileName;
        public XmlParser(string fileName)
        {
            _fileName = fileName;
        }

        public List<Customer> Parse()
        {
            CustomersList data;
            using (var reader = new FileStream($"{_fileName}", FileMode.Open))
                data = (CustomersList)new XmlSerializer(typeof(CustomersList)).Deserialize(reader);

            return data?.Customers;
        }
    }
}
﻿namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public enum ParserTypes      
    {
        Default,
        Xml,
        Csv
    }
}
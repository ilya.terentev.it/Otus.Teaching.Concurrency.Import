﻿using System;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public static class ParserFactory
    {
        public static IDataParser<List<Customer>> GetParser(ParserTypes t, string fileName)
        {
            switch (t)
            {
                case ParserTypes.Xml:
                    return new XmlParser($"{fileName}.xml");
                case ParserTypes.Csv:
                    return new CsvParser($"{fileName}.csv");
                case ParserTypes.Default:
                default:
                    throw new ArgumentOutOfRangeException(nameof(t), t, null);
            }
        }
    }
}

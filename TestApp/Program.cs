﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader;
using Otus.Teaching.Concurrency.Import.XmlGenerator;

namespace TestApp
{
    class Program
    {
        /// <summary>
        /// Запуск генератора файла через создание процесса, сделать возможность выбора в коде,
        /// как запускать генератор, процессом или через вызов метода.
        /// </summary>
        /// <param name="args">
        /// generator mode (GenInProcess, GenInMethod)
        /// thread count
        /// loader type(Fake, Mssql)
        /// parser type(Mssql)
        /// data generator type(Mssql, Csv)
        /// </param>
        static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;  
            var dataFIleName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "data");
            var sw = new Stopwatch();
            sw.Start();
            //var _dataCount = 10;
            if (_genInProc)
            {
                var dgApp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Otus.Teaching.Concurrency.Import.DataGenerator.App.exe");
                var p = Process.Start(dgApp, $"{dataFIleName} {_dataCount} {_dataGeneratorType} ");
                p?.WaitForExit();
            }
            else
            {
                var generator = GeneratorFactory.GetGenerator(_dataGeneratorType, dataFIleName, _dataCount);
                generator.Generate();
            }
            sw.Stop();
            var m = _genInProc ? "Process" : "Method";
            Console.WriteLine($"{_dataCount} records generated (in {m}) to {_dataGeneratorType} in {sw.Elapsed.TotalSeconds} seconds");

            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            var cs = configuration.GetSection("DbInitialSettings").GetValue<string>("ConnectionString");

            var parser = ParserFactory.GetParser(_parserType, $"{dataFIleName}");
            var customers = parser.Parse();

            var l = LoaderFactory.GetLoader(_loaderType, _threadCount, customers, cs);
            sw = new Stopwatch();
            sw.Start();
            l.LoadData();
            sw.Stop();
            Console.WriteLine($"{_dataCount} records load in {sw.Elapsed.TotalSeconds} seconds, by {_threadCount} threads");
            
        }

        private static bool _genInProc;
        private static int _threadCount;
        private static LoaderTypes _loaderType;
        private static ParserTypes _parserType;
        private static GeneratorType _dataGeneratorType;
        private static int _dataCount;
        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args is { Length: > 0 })
            {
                //Запуск генератора файла через создание процесса, сделать возможность выбора в коде, как запускать генератор, процессом или через вызов метода.
                switch (args[0])
                {
                    case "GenInProcess":
                        _genInProc = true;
                        break;
                    case "GenInMethod":
                        _genInProc = false;
                        break;
                    default:
                        Console.WriteLine("The generator mode is not set");
                        return false;
                }
                
                if (args.Length < 2 || !int.TryParse(args[1], out _threadCount))
                {
                    //Хорошо сделать настройку с количеством потоков, чтобы можно было настроить оптимальное количество потоков под размер файла с данными. 
                    Console.WriteLine("The thread count is not set");
                    return false;
                }
                if (args.Length < 3 || !Enum.TryParse(args[2], out _loaderType))
                {
                    Console.WriteLine("The loader type is not set");
                    return false;
                }
                if (args.Length < 4 || !Enum.TryParse(args[3], out _parserType))
                {
                    Console.WriteLine("The parser type is not set");
                    return false;
                }
                if (args.Length < 5 || !Enum.TryParse(args[4], out _dataGeneratorType))
                {
                    Console.WriteLine("The generator type is not set");
                    return false;
                }
                if (args.Length < 6 || !int.TryParse(args[5], out _dataCount))
                {
                    Console.WriteLine("The data count is not set");
                    return false;
                }

            }
            else
            {
                Console.WriteLine("The args is not set");
                return false;
            }


            return true;
        }
    }
}

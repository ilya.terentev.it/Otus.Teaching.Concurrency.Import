﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.WebAPI.Models;

namespace Otus.Teaching.Concurrency.Import.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WebApiController : ControllerBase
    {
        private readonly ILogger<WebApiController> _logger;
        private OtusCustmersDbContext _context;
        public WebApiController(ILogger<WebApiController> logger, OtusCustmersDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        [ProducesResponseType(200 , Type = typeof(ActionResult<IEnumerable<Customer>>))]
        [ProducesResponseType(404)]
        [ProducesResponseType(409)]
        public async Task<ActionResult<IEnumerable<Customer>>> Get(CancellationToken token)
        {
            try
            {
                await using (_context)
                {
                    var a = await _context.Customers.ToListAsync(token);
                    if(!a.Any())
                        return NotFound();
                    return Ok(a);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Problem();
            }
        }
        [Route("users/{id}")]
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(409)]
        public async Task<IActionResult> GetCustomer(int id, CancellationToken token)
        {
            try
            {
                await using (_context)
                {
                    var a = await _context.Customers.FirstOrDefaultAsync(x => x.Id == id, token);
                    if (a != null)
                        return Ok(a);
                    return NotFound();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Problem();
            }
        }
        [Route("users/")]
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(409)]
        public async Task<IActionResult> AddCustomer(Customer model, CancellationToken token)
        {
            try
            {
                await using (_context)
                {
                    var exists = await _context.Customers.AnyAsync(x=>x.Id == model.Id, token);
                    if (exists)
                        return Conflict();
                    
                    await _context.Customers.AddAsync(model, token);
                    await _context.SaveChangesAsync(token);
                    return Ok();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return Problem();
            }
        }
    }
}

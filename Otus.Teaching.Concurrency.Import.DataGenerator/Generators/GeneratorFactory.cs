using System;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(GeneratorType t, string fileName, int dataCount)
        {
            switch (t)
            {
                case GeneratorType.Xml:
                    return new XmlDataGenerator($"{fileName}.xml", dataCount);
                case GeneratorType.Csv:
                    return new CsvGenerator($"{fileName}.csv", dataCount);
                case GeneratorType.Default:
                default:
                    throw new ArgumentOutOfRangeException(nameof(t), t, null);
            }
        }
    }
}
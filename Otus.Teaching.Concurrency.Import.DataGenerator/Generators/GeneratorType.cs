﻿namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public enum GeneratorType
    {
        Default,
        Xml,
        Csv
    }
}

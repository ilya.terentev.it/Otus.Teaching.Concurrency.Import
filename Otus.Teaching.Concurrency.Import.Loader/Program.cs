﻿using System;
using System.Diagnostics;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? string.Empty, "customers.xml");
        
        static void Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            GenerateCustomersDataFile();
            var p = new XmlParser(_dataFilePath);
            var loader = new FakeDataLoader<Customer>(p.Parse(), 12, new JsonRepository("jsonrep.txt"));
            loader.LoadData();
        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new DataGenerator.Generators.XmlGenerator(_dataFilePath, 1000);
            xmlGenerator.Generate();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class MssqlDataLoader<T> : IDataLoader
    {
        private IEnumerable<T> _data;
        private int _threadCount;
        private ICustomerRepository _repository;
        public MssqlDataLoader(IEnumerable<T> data, int threadCount, ICustomerRepository repository)
        {
            _data = data;
            _threadCount = threadCount;
            _repository = repository;
        }   

        public void LoadData()
        {
            //Распараллеливаем обработку файла по набору диапазонов Id, то есть нужно, чтобы файл разбивался на диапазоны по Id и обрабатывался параллельно через Thread
            ThreadPool.SetMaxThreads((int)_threadCount, (int)_threadCount);
            var count = _data.Count() / (int)_threadCount;
            var doneEvents = new List<ManualResetEvent>();
            for (var i = 0; i < _data.Count(); i += count)
            {
                var e = new ManualResetEvent(false);
                doneEvents.Add(e);
                var lngs = Math.Min((int)count, _data.Count() - i);
                var sub = _data.ToList().GetRange(i, lngs);
                var o = new Tuple<List<T>, ManualResetEvent>(sub, e);
                ThreadPool.QueueUserWorkItem(WriteData, o);
                //var t = new Thread(WriteData);
                //t.Start(o);
            }

            WaitHandle.WaitAll(doneEvents.ToArray());
        }

        private void WriteData(object? state)
        {
            Debug.WriteLine($"{Thread.CurrentThread.ManagedThreadId} thread start {DateTime.Now.TimeOfDay}");
            if (state is Tuple<List<T>, ManualResetEvent> tt)
                foreach (var t in tt.Item1)
                    if (t is Customer d)
                        // обработка предполагает сохранение в БД через вызов репозитория. 
                        _repository.AddCustomer(d);

            Debug.WriteLine(
                $"{Thread.CurrentThread.ManagedThreadId} thread finished {DateTime.Now.TimeOfDay}");

            if (state is Tuple<List<T>, ManualResetEvent> tt2)
                tt2.Item2.Set();
        }

        
    }
}
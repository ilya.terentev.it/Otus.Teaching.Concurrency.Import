﻿using System;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public static class LoaderFactory
    {
        public static IDataLoader GetLoader<T>(LoaderTypes t, int threadCount, IEnumerable<T> data, string connectionString = "")
        {
            switch (t)
            {
                case LoaderTypes.Fake:
                    return new FakeDataLoader<T>(data, threadCount, new JsonRepository("jsonrep.txt"));
                case LoaderTypes.Mssql:
                    return new MssqlDataLoader<T>(data, threadCount, new MssqlRepository(connectionString));
                case LoaderTypes.Default:
                default:
                    throw new ArgumentOutOfRangeException(nameof(t), t, null);
            }
        }
    }
}
